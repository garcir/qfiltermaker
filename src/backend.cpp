#include "../include/backend.h"
#include <QList>

Backend::Backend(QObject *parent)
    : QObject{parent}
{
    mAvailableSystems.setStringList({"Input", "LPF", "HPF", "Other"});
}

QStringListModel* Backend::getAvailableSystemsModel()
{
    return &mAvailableSystems;
}

SystemsListModel* Backend::getSetSystemsModel()
{
    return &mSetSystems;
}
