#include "../include/system.h"
#include <QXYSeries>

System::System(QString systemName, QList<QPointF> points, QObject * parent):
    QObject(parent)
{
    mName = systemName;
    mSystemSignal = points;
}

void System::updateSeries(QAbstractSeries *series) const
{
    // For more flexibility in the future
    const QList<QPointF>& srcData = mSystemSignal;
    switch(series->type())
    {
        case(QAbstractSeries::SeriesTypeScatter):
        case(QAbstractSeries::SeriesTypeLine):
        case(QAbstractSeries::SeriesTypeSpline):
        {
            QXYSeries* xySeries = static_cast<QXYSeries*>(series);
            xySeries->clear();
            xySeries->append(srcData);
            break;
        }
        default:
        {
            qDebug() << "Chart type not supported";
            break;
        }
    }
}
