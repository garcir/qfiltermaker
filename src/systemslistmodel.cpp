#include "../include/systemslistmodel.h"

SystemsListModel::SystemsListModel(QObject * parent):
    QAbstractListModel(parent)
{
    //REMOVE TEST
    QList<QPointF> a {
        {0,0},
        {50,50}
    };
    QList<QPointF> b {
        {0,0},
        {20,80}
    };
    mSystemsList.append(new System("Step Signal", a, this));
    mSystemsList.append(new System("HPF", b, this));
    //---
}

int SystemsListModel::rowCount(const QModelIndex &parent) const
{
    return mSystemsList.size();
}

QVariant SystemsListModel::data(const QModelIndex &index, int role) const
{
    if(index.isValid() == false) return QVariant();
    if(index.row() < 0 || index.row() >= rowCount()) return QVariant();

    switch(role)
    {
        case(Qt::DisplayRole): // default to SystemNameRole
        case(SystemNameRole):
        {
            return mSystemsList.at(index.row())->mName;
        }
        case(SystemTypeRole):
        {
        }
    }
    return QVariant();
}

QHash<int, QByteArray> SystemsListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[SystemNameRole] = "name";
    roles[SystemTypeRole] = "type";
    return roles;
}

bool SystemsListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    return true;
}

Qt::ItemFlags SystemsListModel::flags(const QModelIndex &index) const
{
    return Qt::ItemIsEditable;
}


System *SystemsListModel::getSystem(int index)
{
    // TODO!: Create a System member variable or static to pass on these cases, instead of this memory leakage
    if( index > (rowCount() - 1) || index < 0)
    {
        qDebug() << index;
        return new System("empty", QList<QPointF>());
    }
    return mSystemsList.at(index);
}

void SystemsListModel::addSystem(System* sys)
{
   beginInsertRows(QModelIndex(), rowCount(), rowCount() + 1 );
   sys->setParent(this);
   mSystemsList.append(sys);
   endInsertRows();
}
