#ifndef SYSTEM_H
#define SYSTEM_H

#include <QObject>
#include <QString>
#include <QAbstractSeries>

class System: public QObject
{
Q_OBJECT
public:
    System(QString systemName, QList<QPointF> points, QObject * parent = 0);

    Q_INVOKABLE void updateSeries(QAbstractSeries* series) const;

    QString mName;

private:
    QList<QPointF> mSystemSignal;
};

#endif // SYSTEM_H
