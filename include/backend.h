#ifndef BACKEND_H
#define BACKEND_H

#include <QObject>
#include <QStringListModel>
#include "systemslistmodel.h"

class Backend : public QObject
{
    Q_OBJECT
public:
    explicit Backend(QObject *parent = nullptr);

private:
    SystemsListModel mSetSystems;
    QStringListModel mAvailableSystems;

public slots:
    QStringListModel* getAvailableSystemsModel(); // List of class blueprints, when one is selected init instance
    SystemsListModel* getSetSystemsModel();

signals:

};

#endif // BACKEND_H
