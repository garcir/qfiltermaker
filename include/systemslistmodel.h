#ifndef SYSTEMSLISTMODEL_H
#define SYSTEMSLISTMODEL_H

#include <QAbstractListModel>
#include "include/system.h"

class SystemsListModel: public QAbstractListModel
{
    Q_OBJECT
public:
    SystemsListModel(QObject * parent = 0);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QHash<int, QByteArray> roleNames() const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;

    Q_INVOKABLE System* getSystem(int index);
    Q_INVOKABLE void addSystem(System* sys);

signals:

private:
    QList<System*> mSystemsList;

    enum Roles{
        SystemNameRole = Qt::UserRole + 1,
        SystemTypeRole = Qt::UserRole + 2
    };

};

#endif // SYSTEMSLISTMODEL_H
