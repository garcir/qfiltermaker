import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import QtCharts

Window {
    id: root
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    property int groupBoxesHeight: root.height / 5
    property int graphWidth: root.width / 3
    property int graphHeight: root.height / 4

    property var availableSystemsModel : backend.getAvailableSystemsModel()
    property var setSystemsModel: backend.getSetSystemsModel()
    property var selectedSystem: setSystemsModel.getSystem(lw_setSystems.currentIndex)

    GridLayout {
        anchors.fill: parent
        columns: 3
        anchors.margins: 10

        /*
        *   First Row
        */
        Label {
            // Spacer
            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        RowLayout {
            ComboBox {
                model: root.setSystemsModel
                textRole: "display"
            }
            CheckBox {
                text: "Pipeline"
            }
        }

        Label {
            // Spacer
            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        /*
        *   Second Row
        */

        ListView {
            id: lw_setSystems
            Layout.fillWidth: true
            Layout.fillHeight: true

            model: root.setSystemsModel
            delegate: Component {
                Item {
                    width: lw_setSystems.width
                    height: 30
                    Text {
                        text: name
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            lw_setSystems.currentIndex = index
                        }
                    }
                }
            }

            highlight: Rectangle {
                radius: 5
                color: "lightsteelblue"
            }

            highlightFollowsCurrentItem: true
            focus: true
        }

        ColumnLayout {
            Layout.preferredWidth: graphWidth
            Layout.preferredHeight: graphHeight
            Label {
                text: "Timeseries"
            }
            ChartView {          
                Layout.fillWidth: true
                Layout.fillHeight: true

                LineSeries {
                    id: timeSeries
                    name: "Signal"
                    axisX: ValueAxis {
                        min: 0
                        max: 1024
                    }
                    axisY: ValueAxis {
                        min: 0
                        max: 1024
                    }
                    Connections {
                        target: lw_setSystems
                        function onCurrentIndexChanged(index) {
                            selectedSystem.updateSeries(timeSeries)
                        }
                    }
                }
            }
        }

        ColumnLayout {
            Layout.preferredWidth: graphWidth
            Layout.preferredHeight: graphHeight
            Label {
                text: "Spectrum"
            }
            ChartView {
                Layout.fillWidth: true
                Layout.fillHeight: true

                LineSeries {
                    id: lineSeries2
                    name: "Signal"
                    axisX: ValueAxis {
                        min: 0
                        max: 100
                    }
                    axisY: ValueAxis {
                        min: 0
                        max: 100
                    }
                    XYPoint {
                        x: 0
                        y: 0
                    }
                    XYPoint {
                        x: 50
                        y: 50
                    }
                }
            }
        }

        /*
        *   Third Row
        */

        RowLayout {            
            ComboBox {
                id: cb_availableSystems
                model: root.availableSystemsModel
                textRole: "display"
            }
            Button {
                text: "Add"
                //onClicked: backend.addToSetSystemsModel(cb_availableSystems.currentText)
            }
        }

        Label {
            //Spacer
            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        ComboBox {
            model: ["text"]
        }

        /*
        *   Fourth Row
        */

        GroupBox {
            Layout.fillWidth: true
            Layout.preferredHeight: groupBoxesHeight
            title: "Filter Info"

            ColumnLayout {
                anchors.fill: parent
                Rectangle {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    color: "red"
                }
            }
        }

        GroupBox {
            Layout.fillWidth: true
            Layout.preferredHeight: groupBoxesHeight
            title: "Signal Info"


            ColumnLayout {
                anchors.fill: parent
                Rectangle {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    color: "blue"
                }
            }
        }
        Button {
           Layout.alignment: Qt.AlignBottom | Qt.AlignRight
           text: "save"
        }
    }
}
